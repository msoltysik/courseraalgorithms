/**
 * Mateusz Sołtysik
 * Copyright, 2014.
 */
public class Subset {
    public static void main(String[] args) {
        RandomizedQueue<String> randomizedQueue = new RandomizedQueue<String>();

        while (StdIn.hasNextLine() && !StdIn.isEmpty()) {
            randomizedQueue.enqueue(StdIn.readString());
        }

        int n = Integer.parseInt(args[0]);

        for (int i = 0; i < n; i++) {
            System.out.println(randomizedQueue.dequeue());
        }
    }
}