import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Mateusz Sołtysik
 * Copyright, 2014.
 */
public class RandomizedQueueTest {
    private RandomizedQueue<String> randomizedQueue;

    @Before
    public void setUp() throws Exception {
        randomizedQueue = new RandomizedQueue<String>();
    }

    @After
    public void tearDown() throws Exception {
        randomizedQueue = null;

    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(randomizedQueue.isEmpty());

        randomizedQueue.enqueue("First element");
        assertFalse(randomizedQueue.isEmpty());
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(0, randomizedQueue.size());

        randomizedQueue.enqueue("First element");
        assertEquals(1, randomizedQueue.size());

        randomizedQueue.enqueue("Second element");
        assertEquals(2, randomizedQueue.size());
    }

    @Test
    public void testEnqueue() throws Exception {
        String[] strings = new String[]{"First element.", "Second element."};
        for (String s : strings) {
            randomizedQueue.enqueue(s);
        }

        Iterator<String> iterator = randomizedQueue.iterator();
        assertEquals("Second element.", iterator.next());
        assertEquals("First element.", iterator.next());

    }

    @Test
    public void testDequeue() throws Exception {
        String[] strings = new String[]{"First element.", "Second element."};
        for (String s : strings) {
            randomizedQueue.enqueue(s);
        }

        assertEquals(2, randomizedQueue.size());

        randomizedQueue.dequeue();
        assertEquals(1, randomizedQueue.size());

        randomizedQueue.dequeue();
        assertEquals(0, randomizedQueue.size());
    }

    @Test
    public void testSample() throws Exception {
        String[] strings = new String[]{"First element.", "Second element."};
        for (String s : strings) {
            randomizedQueue.enqueue(s);
        }
        assertTrue(Arrays.asList(strings).contains(randomizedQueue.sample()));

    }

    @Test
    public void testIterator() throws Exception {
        String[] strings = new String[]{"First element.", "Second element."};
        for (String s : strings) {
            randomizedQueue.enqueue(s);
        }

        Iterator<String> it = randomizedQueue.iterator();
        Throwable e = null;

        try {
            it.remove();
        } catch (UnsupportedOperationException ex) {
            e = ex;
        }

        assertNotNull(e);
    }
}
